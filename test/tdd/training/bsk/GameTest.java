package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {

	@Test
	public void testShouldReturnRightIndex() throws BowlingException{
		
		Game game=new Game();
		Frame frame=new Frame(1,5);
		int index=0;
		
		game.addFrame(frame);
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(frame,game.getFrameAt(index));
	}
	
	@Test
	public void testShouldReturnGameScore() throws BowlingException{
		
		Game game=new Game();
		Frame frame=new Frame(1,5);
		game.addFrame(frame);
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(81,game.calculateScore());
	}
	
	@Test
	public void testShouldReturnGameScoreWithSpare() throws BowlingException{
		
		Game game=new Game();
		Frame frame=new Frame(1,9);
		game.addFrame(frame);
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(88,game.calculateScore());
	}
	
	@Test
	public void testShouldReturnGameScoreWithStrike() throws BowlingException{
		
		Game game=new Game();
		Frame frame=new Frame(10,0);
		game.addFrame(frame);
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(94,game.calculateScore());
	}
	
	@Test
	public void testShouldReturnGameScoreWithStrikeAndSpare() throws BowlingException{
		
		Game game=new Game();
		Frame frame=new Frame(10,0);
		game.addFrame(frame);
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(103,game.calculateScore());
	}
	
	@Test
	public void testShouldReturnGameScoreWithMultipleStrike() throws BowlingException{
		
		Game game=new Game();
		Frame frame=new Frame(10,0);
		game.addFrame(frame);
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(112,game.calculateScore());
	}
	
	@Test
	public void testShouldReturnGameScoreWithMultipleSpare() throws BowlingException{
		
		Game game=new Game();
		Frame frame=new Frame(8,2);
		game.addFrame(frame);
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		assertEquals(98,game.calculateScore());
	}
	
	@Test
	public void testShouldReturnBonusThrowWithSpare() throws BowlingException{
		
		Game game=new Game();
		Frame frame=new Frame(1,5);
		int firstBonusThrow=7;
		game.addFrame(frame);
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		game.setFirstBonusThrow(firstBonusThrow);
		assertEquals(firstBonusThrow,game.getFirstBonusThrow());
	}
	
	@Test
	public void testShouldReturnScoreWithFirstBonusThrow() throws BowlingException{
		
		Game game=new Game();
		Frame frame=new Frame(1,5);
		int firstBonusThrow=7;
		game.addFrame(frame);
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		game.setFirstBonusThrow(firstBonusThrow);
		assertEquals(90,game.calculateScore());
	}
	
	@Test
	public void testShouldReturnBonusThrowWithStrike() throws BowlingException{
		
		Game game=new Game();
		Frame frame=new Frame(1,5);
		int firstBonusThrow=7;
		int secondBonusThrow=2;
		game.addFrame(frame);
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(firstBonusThrow);
		game.setSecondBonusThrow(secondBonusThrow);
		assertEquals(secondBonusThrow,game.getSecondBonusThrow());
	}
	
	@Test
	public void testShouldReturnScoreStrikeAsLastFrame() throws BowlingException{
		
		Game game=new Game();
		Frame frame=new Frame(1,5);
		int firstBonusThrow=7;
		int secondBonusThrow=2;
		game.addFrame(frame);
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(firstBonusThrow);
		game.setSecondBonusThrow(secondBonusThrow);
		assertEquals(92,game.calculateScore());
	}
	
	@Test
	public void testShouldReturnBestScore() throws BowlingException{
		
		Game game=new Game();
		Frame frame=new Frame(10,0);
		int firstBonusThrow=10;
		int secondBonusThrow=10;
		game.addFrame(frame);
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.setFirstBonusThrow(firstBonusThrow);
		game.setSecondBonusThrow(secondBonusThrow);
		assertEquals(300,game.calculateScore());
	}

}
