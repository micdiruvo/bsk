package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test
	public void testShouldReturnFirstThrow() throws BowlingException{
		int firstThrow=2;
		int secondThrow=4;
		Frame frame= new Frame(firstThrow,secondThrow);
		assertEquals(2,frame.getFirstThrow());
	}
	
	@Test
	public void testShouldReturnSecondThrow() throws BowlingException{
		int firstThrow=2;
		int secondThrow=4;
		Frame frame= new Frame(firstThrow,secondThrow);
		assertEquals(4,frame.getSecondThrow());
	}
	
	@Test
	public void testShouldReturnFrameScore() throws BowlingException{
		int firstThrow=2;
		int secondThrow=6;
		Frame frame= new Frame(firstThrow,secondThrow);
		assertEquals(8,frame.getScore());
	}
	
	@Test
	public void testShouldReturnBonus() throws BowlingException{
		int firstThrow=1;
		int secondThrow=9;
		int firstThrow2=3;
		Frame frame= new Frame(firstThrow,secondThrow);
		frame.setBonus(firstThrow2);
		assertEquals(3,frame.getBonus());
	}
	
	@Test
	public void testShouldReturnSpare() throws BowlingException{
		int firstThrow=1;
		int secondThrow=9;
		Frame frame= new Frame(firstThrow,secondThrow);
		assertEquals(true,frame.isSpare());
	}
	
	@Test
	public void testShouldReturnStrike() throws BowlingException{
		int firstThrow=10;
		int secondThrow=0;
		Frame frame= new Frame(firstThrow,secondThrow);
		assertEquals(true,frame.isStrike());
	}

}
