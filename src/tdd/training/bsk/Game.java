package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	
	private ArrayList<Frame> game=new ArrayList<Frame>();
	public int MAXFRAME=10;
	private int firstBonusThrow;
	private int secondBonusThrow;
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		// To be implemented
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		game.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		// To be implemented
		return game.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		// To be implemented
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		// To be implemented
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int GameScore=0;
		
		for(int i=0;i<MAXFRAME;i++) {
			GameScore+=game.get(i).getScore();
			boolean strikeFlag=false;
			 if(game.get(i).isStrike()) {
				 game.get(i).setBonus(game.get(i+1).getScore());
				 if(i==MAXFRAME-1) {
						GameScore+=getFirstBonusThrow()+getSecondBonusThrow(); 
				 }
				 if(i==MAXFRAME-2) {
					
					GameScore+=game.get(i).getBonus()+getFirstBonusThrow();
				 }
				 if(i<MAXFRAME-2) {
					
					GameScore+=game.get(i).getBonus();
					if(game.get(i+1).isStrike()) {
						game.get(i).setBonus(game.get(i+2).getFirstThrow());
						GameScore+=game.get(i).getBonus();
					}
				}
				strikeFlag=true;
			}
			if(game.get(i).isSpare()  && strikeFlag==false) {
				if( i<MAXFRAME-1) 
					game.get(i).setBonus(game.get(i+1).getFirstThrow());
				else
					game.get(i).setBonus(this.getFirstBonusThrow());
				GameScore+=game.get(i).getBonus();
			}
		}
		return GameScore;	
	}
}
